<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/estilo.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="shortcut icon" type="imagem/x-icon" href="https://www.navirai.ms.gov.br/wp-content/themes/site/assets/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Prefeitura</title>
</head>

<body>
    <!-- Cabeçalho -->
    
    <nav class="navbar navbar-expand-lg">
        <!-- logo-mobile -->
        <a href="https://www.navirai.ms.gov.br">
            <h1>
                <img id="img-logo" class="img-responsive img-fluid" src="https://www.navirai.ms.gov.br/wp-content/themes/site/assets/imagens/brasao-texto-branco0.png" alt="Prefeitura Municipal de Naviraí">
            </h1>
        </a>
        <!-- /logo-mobile -->
        <!--  menu-mobile -->
        <span class="open-slide right">
            <a href="#" onclick="openSlideMenu()">
                <i class="fas fa-bars hamb"></i>
            </a>
        </span>
        <!-- /menu-mobile -->
    </nav>
    
    <!-- titulo -->
    <div class="header-img">
        <div id="titulo"><h1>CARTA DE <span class="bold">SERVIÇOS</span></h1></div>
    </div>
    <!-- /titulo -->
    <!-- /cabeçalho -->
    <div id="side-menu" class="side-nav">
        <a href="#" class="btn-close" onclick="closeSlideMenu()">&times;</a>
        <a href="">Home</a>
        <a href="">About</a>
        <a href="">Services</a>
        <a href="">Contact</a>
    </div>
    <div class="content p-0">
        <div class="menu container-fluid padding-0">
            <div class="row">
                <div id="servidor"  class="col-3 d-flex flex-row justify-content-center">
                    <div class="d-flex flex-column align-self-center icone">
                        <i class="fas fa-user-tie"></i>
                        <span class=" texto-icone">SERVIDOR</span>
                    </div>
                </div>
                <div id="empresa" class="col-3 d-flex flex-row justify-content-center">
                    <div class="d-flex flex-column align-self-center icone">
                        <i class="fas fa-building"></i>
                        <span class="texto-icone">EMPRESA</span>
                    </div>
                </div>
                <div id="cidadao" class="col-3 d-flex flex-row justify-content-center">
                    <div class="d-flex flex-column align-self-center icone">
                        <i class="fas fa-users"></i>
                        <span class="texto-icone">CIDADÃO</span>
                    </div>
                </div>
                <div id="buscar" class="col-3 d-flex flex-row justify-content-center">
                    <div class="d-flex flex-column align-self-center icone">
                        <i class="fas fa-search"></i>
                        <span class="texto-icone">BUSCAR</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="temas">
            <div class="row">
                <div class="col-3 quem-sou">
                    <span>CIDADÃO</span>
                </div>
                <div class="col-9">
                    <span>ESCOLHA O TEMA</span>
                </div>  
            </div> 
        </div>
        <div class="pesquisa container-fluid">
            <form action="" method="get">
                <div class="input-group mb-1 ">
                    <input type="text" class="  form-control" placeholder="PALAVRA-CHAVE"  aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-primary">Pesquisar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="container-fluid conteudo mt-3 p-0">
            <div class="row">
                <h2 class="mais-acessos">Serviços mais acessados:</h2>
            </div>
            <div class="row mt-2 item-servicos">
                <div class="col-md-3 p-0">
                    <img src="assets/img/icones/assistencia.png" class="" >
                </div>
                <div class="col-md-9 p-3">
                    <p class="font-14 bold azul">ASSISTÊNCIA SOCIAL</p>
                    <h3>Abordagem Social - Pessoas em Situação de Rua nos Espaços Públicos.</h3>
                    <p class ="font-16">Identificação de pessoas em situação de risco</p>
                </div>
            </div>    
            <div class="row mt-2 item-servicos">
                <div class="col-md-3 p-0">
                    <img src="assets/img/icones/documentos.png" class="" >
                </div>
                <div class="col-md-9 p-3">
                    <p class="font-14 bold azul">DOCUMENTOS, LICENÇAS E AUTORIZAÇÕES</p>
                    <h3>Alvará de Saúde, Certidões e Documentos - Estabelecimentos Veterinários</h3>
                    <p class ="font-16">Emissão e renovação de alvarás de saúde para estabelecimentos do ramo de serviços veterinários</p>
                </div>
            </div>
            <div class="row mt-2 item-servicos">
                <div class="col-md-3 p-0">
                    <img src="assets/img/icones/industrias.png" class="" >
                </div>    
                <div class="col-md-9 p-3">
                    <p class="font-14 bold azul">INDÚSTRIA E COMÉRCIO</p>
                    <h3>Alvará</h3>
                    <p class="font-16">Licença que autoriza o funcionamento de uma empresa da indústria, comércio ou serviços</p>
                </div>
            </div>
            <div class="row mt-2 item-servicos">
                <div class="col-md-3 p-0">
                    <img src="assets/img/icones/imposto.png" class="" >
                </div>
                <div class="col-md-9 p-3">
                    <p class="font-14 bold azul">IMPOSTOS E TAXAS</p>
                    <h3>Certidão de Débitos Tributários</h3>
                    <p class="font-16">Emissão de certidões de débitos relativos aos tributos municipais: IPTU/TCL, ITBI, ISSQN e TFLF.</p>
                </div>
            </div>
        </div>
        <div class=" row p-3 redes-sociais">
            <div class="d-flex flex-row align-content-center col-12 p-3">
                <p class="social-centralizado bold">Redes Sociais da Prefeitura de Naviraí </p>
            </div>
            <div class="col-12 ">
                <p class="social-centralizado"><span><i class="fab fa-facebook-square"></i></span> <span><i class="fab fa-youtube"></i></span>  <span><i class="fab fa-instagram"></i></span> </p>
            </div>
        </div>
        <div class=" row p-3 contatos">
            <div class="d-flex flex-column align-content-center col-12 p-3">
                <h2 class="centralizado bold">Endereço</h2>
                <p class="centralizado">
                Prefeitura Municipal de Naviraí - MS<br/>
                Praça Prefeito Euclides Antônio Fabris, 343 - Centro    
                </p>
                <h2 class="centralizado bold">Fale Conosco</h2>    
                <p class="centralizado">
                imprensa@navirai.ms.gov.br<br/>
                Fone/Fax: 3409-1500    
                </p>
            </div>
        </div>
        <div class="row p-3 rodape-mobile">
            <div class="d-flex flex-column align-content-center col-12 p-2">
                <p class ="centralizado bold font-14 mb-2">
                    Desenvolvido pelo Núcleo de Informática
                </p>
                <img src="http://10.0.0.10/logo-nti.png" alt="Núcleo de informática" >
            </div>
        </div>
    </div>
</body>

<script>
        function openSlideMenu() {
            document.getElementById('side-menu').style.width = '250px';
        }

        function closeSlideMenu() {
            document.getElementById('side-menu').style.width = '0';
        }
</script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>